package turinix.company.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TurinixApplication {

	public static void main(String[] args) {
		SpringApplication.run(TurinixApplication.class, args);
	}

}
